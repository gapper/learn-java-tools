package com.gao.test;

import java.io.IOException;

/**
 * @author gao
 * @time 2020/04/2020/4/22 21:08:58
 */
public class Demo03 {
    /*
        -Xms20M -Xmx20M -Xmn10M -XX:+UseSerialGC -XX:+PrintGCDetails -verbose:gc

            -Xms 20M                    指定堆的最小容量
            -Xmx 20M                    指定堆的最大容量
            -Xmn 10M                    指定年轻代的大小
            -XX:+UseSerialGC            指定垃圾收集器为SerialGC
            -XX:+PrintGCDetails -verbose:gc 如果发生了GC行为，则会把GC的详细信息打印出来
     */
    public static void main(String[] args) throws IOException {
        final int _1MB = 1024 * 1024;
        byte[] b1 = new byte[2 * _1MB];
        System.out.println("1...");
        System.in.read();

        byte[] b2 = new byte[2 * _1MB];
        System.out.println("2...");
        System.in.read();

        byte[] b3 = new byte[2 * _1MB];
        System.out.println("3...");
        System.in.read();
    }
}


/*
    键入：
    jstat -gc 58264

    输出：
    S0C    S1C    S0U    S1U      EC       EU        OC         OU       MC     MU    CCSC   CCSU   YGC     YGCT    FGC    FGCT     GCT
    1024.0 1024.0  0.0    0.0    8192.0   5195.7   10240.0      0.0     4480.0 770.3  384.0   75.9       0    0.000   0      0.000    0.000

    SOC     幸存区容量
    S1C     幸存区容量
    S0U     幸存区使用量
    S1U     幸存区使用量
    EC      eden容量
    EU      eden使用量
    OC      老年代容量
    OU      老年代使用量
    MC      元空间容量（等价于1.7中的方法区）
    MU      元空间使用量
    CCSC    压缩类空间的容量
    CCSU    压缩类空间的使用量
    YGC     young gc的次数
    YGCT    young gc所花费的时间
    FGC     full gc的次数
    FGCT    full gc所花费的时间
    GCT     所有垃圾回收花费的总时间


    键入：
    jstat -gcutil 58264
    输出（百分比）：
    S0     S1     E      O      M     CCS    YGC     YGCT    FGC    FGCT     GCT
    0.00   0.00  63.42   0.00  17.19  19.76  0       0.000     0    0.000    0.000

 */
