package com.gao.test;

import java.io.IOException;

/**
 * @author gao
 * @time 2020/04/2020/4/22 21:41:17
 *
 * jinfo，可以在虚拟机运行时，动态地获取、设置参数。
 *
 * 命令：jinfo pid
 * 描述：输出当前jvm进程的全部参数和系统属性
 *
 * 命令：jinfo -flags pid
 * 描述：输出全部的虚拟机参数
 *
 * 命令：jinfo -sysprops pid
 * 描述：输出当前虚拟机进程的全部系统属性
 *
 * 命令：jinfo -flag name pid
 * 描述：使用该命令，可以查看指定的jvm参数的值
 * 比如，查看当前jvm进程是否开启打印GC日志的功能：jinfo -flag PrintGC pid
 *
 * 命令：jinfo -flag [+|-]name pid
 * 描述：开启或者关闭对应名称的参数
 * 使用jinfo可以在不重启虚拟机的情况下，动态地修改jvm的参数。尤其是在线上的环境特别有用。
 * jinfo -flag +PrintGC pid
 * jinfo -flag PrintGC pid
 * jinfo -flag -PrintGC pid
 * jinfo -flag PrintGC pid
 *
 * 命令：jinfo -flag name=value pid
 * 描述：修改指定参数的值。
 * 和上面的例子相似，但是上面的主要是针对boolean类型的参数进行设置的。
 * 如果是设置value值，则需要使用name=value的形式
 * jinfo -flag HeapDumpPath pid
 * jinfo -flag HeapDumpPath=d:\\dump pid
 * jinfo -flag HeapDumpPath pid
 * jinfo -flag HeapDumpPath= pid
 * jinfo -flag HeapDumpPath pid
 *
 * 注意：jinfo虽然可以在java程序运行时动态地修改虚拟机参数，但并不是所有的参数都支持动态修改。
 */
public class Demo04 {
    public static void main(String[] args) throws IOException {
        System.out.println("jinfo");
        System.in.read();

    }
}
