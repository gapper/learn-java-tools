package com.gao.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author gao
 * @time 2020/04/2020/4/23 21:07:47
 *
 * JConsole
 * 作用：查看java应用程序的运行概况，监视垃圾收集器管理的虚拟机内存（堆和元空间）的变化趋势，以及监控程序内的线程。
 *
 * 启动JConsole：
 * 在控制台输入：jconsole即可，在弹出的界面中，选择本地进程，然后进去看界面页签信息。显示的是整个虚拟机运行的概览。
 * 其中包括堆内存使用情况，线程，类，CPU使用情况。
 *
 * 内存：
 * 相当于命令行的jstat命令，用于监视虚拟机内存（堆和元空间）的变化趋势，这不仅是包括堆内存的整体信息，更细化到伊甸
 * 区、幸存区，老年代的使用情况。同时，也包括非堆区，即元空间的使用情况。单机界面右上角的“执行GC”按钮，可以强制
 * 应用程序进行一次Full GC。
 *
 * 线程：
 * 相当于命令行的jstack命令，遇到线程停顿的时候，可以使用它来进行监控分析。JConsole显示了系统内的线程数量，并在屏幕
 * 下方，显示了程序中所有的线程。单击线程名称，便可以查看线程的栈信息。
 *
 * 类：
 * 显示了系统以及装载的类数量。在详细信息栏中，还显示了已卸载的类数量。
 *
 * VM摘要：
 * 在VM摘要页面，JConsole显示了当前应用程序的运行环境。包括虚拟机类型、版本、堆信息以及虚拟机参数等。相当于jinfo命令
 *
 * MBean：
 * MBean页面允许通过JConsole访问已经在MBean服务器注册的MBean对象。
 *
 */
public class Demo08 {
    public static void main(String[] args) throws InterruptedException, IOException {
        // 程序一开始，阻塞5s
        Thread.sleep(5000);
        System.out.println("start...");

        test(10000);

        System.out.println("Enter...");
        System.in.read();
    }

    private static void test(int num) throws InterruptedException {
        final int _128k = 128 * 1024; // 128k
        List<byte[]> list = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            // 稍作延时，令监视曲线的变化更加明显
            Thread.sleep(100);
            list.add(new byte[_128k]);
        }

    }
}





























