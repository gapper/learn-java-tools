package com.gao.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author gao
 * @time 2020/04/2020/4/23 22:19:24
 * <p>
 * Visual VM
 * Visual VM是到目前为止随JDK发布的功能最强大的运行监视和故障处理程序，并且在未来一段时间内都是官方主力发展的虚拟机故障处理工具。
 * 官方在Visual VM的软件说明中写上了“All-in-one”的描述字样，预示着它除了运行监视、故障处理外，还提供了很多其他方面的功能。如性
 * 能分析，Visual VM的性能分析功能甚至比起很多专业的收费工具都不会逊色多少，而且Visual VM还有一个很大的有点，就是不需要被监视的
 * 程序基于特殊的环境而运行，因此它对应用程序的实际性能的影响很小，使得它可以直接应用在生产环境中。
 * <p>
 * 使用;
 * 1. 在控制台输入：jvisualvm即可执行；
 * 2. 安装插件：
 * 2.1 从主菜单中选择 工具 -> 插件
 * 2.2 在“可用插件”标签中，选中该插件的“安装”复选框。单机“安装”；
 * 2.3 逐步完成插件安装程序。
 */
public class Demo10 {

    // jvisualv：内存分析
    public static void main(String[] args) throws IOException, InterruptedException {
        test();
        System.out.println("Enter...");
        System.in.read();
    }

    private static void test() throws InterruptedException {
        List<Foo> list = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            Thread.sleep(1000);
            list.add(new Foo());
        }
    }
}

class Foo {
    private byte[] big = new byte[5 * 1024 * 1024]; // 5M
}