package com.gao.test;

import java.io.IOException;

/**
 * @author gao
 * @time 2020/04/2020/4/22 22:46:02
 *
 * jstack（Stack Trace for java）
 * 作用: 查看或导出Java应用程序中线程堆栈信息。
 * 线程快照是当前JVM内，所有条线程正在执行的方法堆栈的集合，生成线程快照的主要目的是定位线程出现长时间停顿的原因，
 * 如线程间发生了死锁、线程死循环、线程长时间等待外部资源等等。
 *
 * 线程出现停顿的时候，可以通过jstack来查看各个线程的调用堆栈，就可以知道没有响应的线程到底在后台做什么事情，或者
 * 在等待什么资源。如果java程序崩溃，进而生成了core文件，jstack工具可以用来获得core文件的java stack和native stack
 * 的信息，从而可以轻松地知道java程序是如何崩溃和在程序何处发生问题。另外，jstack工具还可以附属到正在运行的java程
 * 序中，看到当时运行的java程序的java stack和native stack的信息。
 *
 * 命令格式：
 * jstack [options] <pid>
 *
 * jstak:
 *      -F: 当线程挂起时，使用jstack pid请求不被响应时，强制输出线程堆栈
 *      -l：处堆栈外，显示关于锁的附加信息，例如ownable synchronizers
 *      -m：可以同时输出java以及C/C++的堆栈信息
 *
 */
public class Demo06 {
    public static void main(String[] args) throws IOException {
        System.out.println("jstack");
        System.in.read();
    }
}
