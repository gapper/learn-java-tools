package com.gao.test;

import java.io.IOException;

/**
 * @author gao
 * @time 2020/04/2020/4/22 22:05:41
 *
 * jmap(Memory Map for Java)
 * jmap是一个多功能的命令，它可以生产java程序的dump文件，也可以查看堆内对象的信息、查看ClassLoader的信息以及finalizer队列
 *
 * 命令：jmap pid
 * 描述：查看进程的内存映像信息
 * 使用不带选项参数的jmap打印共享对象映射，将会打印目标虚拟机中加载的每个共享对象的起始地址，映射大小以及共享对象文件的路径全称
 *
 * 命令：jmap -heap pid
 * 描述：显示Java堆的详细信息
 * 打印一个堆的摘要信息，包括使用的GC算法、堆配置信息和各内存区域内存使用信息
 *
 * 命令：jmap -histo:live pid
 * 命令：jmap -histo pid
 * 描述：显示堆中对象的统计信息
 * 其中包括每个Java类、对象数量、内存大小（单位：字节）、全限定的类名。打印的虚拟机内部的类名称将会带有一个‘*’前缀。
 * 如果指定了live子选项，则只计算活动的对象
 *
 * 命令：jmap -clstats pid
 * 描述：打印类加载器信息
 * -clstats是-permstat的代替方案，在JDK8之前，-permstat用来打印类加载器的数据
 *
 * 命令：jmap -finalizerinfo pid
 * 描述：打印等待终结的对象信息
 * Number of objects pending for finalization:0 说明当前F-Queue队列中并没有等待finalizer线程执行finalizer方法的对象
 *
 * 命令：jmap -dump:live,format=b,file=jmap.bin pid
 * 描述：生成堆转储快照dump文件
 * format=b指定，以二进制格式转储Java堆到指定的文件中。live子选项是可选的。如果指定了live子选项，堆中只有活动的对象会被转储。
 *
 *
 * jhat(JVM Heap Dump Browser)
 * 作用：与jmap搭配使用来分析jmap生成的堆转储快照。jhat内置了一个微型的HTTP/HTML服务器，生成dump文件的分析结果后，可以在浏览器中查看。
 * 注意，一般不使用该工具，除非实在没有其他工具了才使用。主要原因有两个：一是不会在部署应用程序的服务器上直接分析dump文件，而是尽量将
 * dump文件拷贝到其他机上进行分析，因为分析的过程是一个十分消耗资源的过程。二是jhat的分析功能相对来说比较简陋，所以会使用其他较好的代替工具，
 * 比如VisualVM，以及专业的用于分析dump文件的Eclipse Memory Analyzer、IBM HeapAnalyzer等工具都能实现比jhat更强大更专业的分析功能。
 *
 *  命令：jhat E:/jmap.bin， 然后使用浏览器访问：http://localhost:7000/
 *
 */
public class Demo05 {
    public static void main(String[] args) throws IOException {
        System.out.println("jmap");
        System.in.read();
    }
}
